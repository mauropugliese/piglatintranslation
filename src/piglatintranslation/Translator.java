package piglatintranslation;

public class Translator {
	
	private String phrase;
	String phraseComplete ="";
	public static final String NIL = "nil";
	
	public Translator(String inputPhrase) {
		phrase= inputPhrase;
	}

	
	
	public String getPhrese() {
		return phrase;
	}



	public String translate() {
		
		
		if(!phrase.isEmpty()) {
			String[] word = wordsSeparatedByDashOrSpace(phrase);
			
			if((word.length<=1)) {
				 word[0]=phrase;
			}
			
			for(int i=0; i<word.length; i++) {
				word[i]=getPhraseWithMultipleEnds(word[i]);
				
				word[i]=wordsContainingPunctuations(word[i]);
					if(phraseComplete.isEmpty()) {
						phraseComplete= word[i];
					}
					
					else if(phrase.contains(" ")){
						phraseComplete= phraseComplete +" " + word[i];	
					}
					
					else if(phrase.contains("-")) {
						phraseComplete= phraseComplete +"-" + word[i];
					}	
				}
				return phraseComplete;
	
		}
		
		
		return NIL;
				
	}
	
	
	
	
	private boolean startWithVowel(String word) {
		return word.startsWith("a") || word.startsWith("e") || word.startsWith("i") || word.startsWith("o") || word.startsWith("u");
			
		
	}
	
	private boolean endingWithVowel(String word) {
		return word.endsWith("a") || word.endsWith("e") || word.endsWith("i") || word.endsWith("o") || word.endsWith("u");
			
		
	}


	private boolean endingWithConsonant(String word) {
		return !(word.endsWith("a") || word.endsWith("e") || word.endsWith("i") || word.endsWith("o") || word.endsWith("u"));
			
		
	}
	
	private String getPhraseWithMultipleEnds(String word){
		
		if(startWithVowel(word)){
			
			if(word.endsWith("y")) {
				word =word + "nay";
			}
			
			else if (endingWithVowel(word)) {
				word =word + "yay";	
			}
			
			else if(startWithVowel(word) && endingWithConsonant(phrase)) {
				word =word + "ay";
			}
			
		}
		else if(startWithOneOrMoreConsonants(word)) {
			while(startWithOneOrMoreConsonants(word)) {
				word= word.substring(1) + word.substring(0,1);	
			}
			word = word + "ay";
		}
				
		return word;
	}
	
	

	
	private boolean startWithOneOrMoreConsonants(String word) {
		return !(word.startsWith("a") || word.startsWith("e") || word.startsWith("i") || word.startsWith("o") || word.startsWith("u"));
		
	}
	
	private String[] wordsSeparatedByDashOrSpace (String phrase) {
		String[] word = {""};
		if( phrase.contains("-")) {
			word = phrase.split("-");
			
		}	
	    if ( phrase.contains(" ")) {
			word = phrase.split(" ");	
		}
		
		return word;
	}
	
	private String wordsContainingPunctuations(String words) {
		
	
			if(words.contains(".")) {
			words=words.replace(".","") + ".";
			}
			
			if(words.contains(",")) {
				words=words.replace(",","") + ",";
			}
			
			if(words.contains(";")) {
				words=words.replace(";","") + ";";
			}
			
			if(words.contains(":")) {
				words=words.replace(":","") + ":";
			}
			
			if(words.contains("?")) {
				words=words.replace("?","") + "?";
			}
			
			if(words.contains("!")) {
				words=words.replace("!","") + "!";
			}
			
			if(words.contains("'")) {
				words=words.replace("'","") + "'";
			}
			
			if(words.contains("(")) {
				words=words.replace("(","") + "(";
			}
			
			if(words.contains(")")) {
				words=words.replace(")","") + ")";
			}
			
			return words;
			
	}
		
}
	

